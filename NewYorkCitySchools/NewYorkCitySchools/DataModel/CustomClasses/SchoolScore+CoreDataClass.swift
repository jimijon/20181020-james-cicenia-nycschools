//
//  SchoolScore+CoreDataClass.swift
//  
//
//  Created by James Cicenia on 10/20/18.
//
//

import Foundation
import CoreData
import Moya
import Sync


@objc(SchoolScore)
public class SchoolScore: NSManagedObject {

    class func syncNYCData(dataStack:DataStack){
        
        let provider = MoyaProvider<NYCSchoolDataService>()
        provider.request(.getSchoolScores ){ result in
            switch result {
            case let .success(moyaResponse):
                do {
                    let context = dataStack.newNonMergingBackgroundContext()
                    
                    try _ = moyaResponse.filterSuccessfulStatusCodes()
                    
                    let data = try moyaResponse.mapJSON()
                    
                    context.perform {
                        
                        Sync.changes(
                            data as! [[String : String]],
                            inEntityNamed: "SchoolScore",
                            dataStack: dataStack,
                            operations: [Sync.OperationOptions.insert, Sync.OperationOptions.update,
                                         Sync.OperationOptions.delete,]) {
                                            error in
                                            print(error ?? "School Scores Imported")
                                            
                                            School.postProcessImport(context:context)
                                            
                        }
                        
                    }
                    
                }
                catch {
                    // NOOP
                    // Swallow the error as we have a seed file.
                    // If this was necssary send a notification
                    
                }
            // do something in your app
            case let .failure(error):
                print(" \(error)")
                // NOOP
                // Swallow the error as we have a seed file.
                // If this was necssary send a notification
            }
        }
    }
}
