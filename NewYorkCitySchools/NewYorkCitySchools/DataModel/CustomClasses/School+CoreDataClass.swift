//
//  School+CoreDataClass.swift
//  
//
//  Created by James Cicenia on 10/20/18.
//
//

import Foundation
import CoreData
import Moya
import SwiftyJSON
import Sync

@objc(School)
public class School:NSManagedObject {

    /* let's create a stored sequencekey */
    override public func willSave() {
        super.willSave()

        if self.changedValues()["school_name"] == nil {
            let key  = "\(school_name!.uppercased().first!)"
            if self.sectionKey != key {
                self.sectionKey =  key 
                print(key)
            }
        }
        
    }
    
    /*  This  coombines the Alamofire/Moya/Sync frameworks to query
     *  the NYC Rest API. Thing to note is how  the mapping was done  to indicate  dbn  is the key.
     *  that is  done  it they coredata editor for the dbn property
     */
    
    class func syncNYCData(dataStack:DataStack){
        
        let provider = MoyaProvider<NYCSchoolDataService>()
        provider.request(.getSchools ){ result in
            switch result {
            case let .success(moyaResponse):
                do {
                    let context = dataStack.newNonMergingBackgroundContext()
                    
                    try _ = moyaResponse.filterSuccessfulStatusCodes()
                    
                    let data = try moyaResponse.mapJSON()
                    
                        context.perform {
                            
                            Sync.changes(
                                data as! [[String : String]],
                                inEntityNamed: "School",
                                dataStack: dataStack,
                                operations: [Sync.OperationOptions.insert, Sync.OperationOptions.update,
                                    Sync.OperationOptions.delete,]) {
                                        error in
                                        print(error ?? "Schools Imported")
                                        SchoolScore.syncNYCData(dataStack: dataStack)
                            }
                            
                        }
                    
                }
                catch {
                    // NOOP
                    // Swallow the error as we have a seed file.
                    // If this was necssary send a notification
                    
                }
            // do something in your app
            case let .failure(error):
                // TODO: handle the error == best. comment. ever.
                print("\(error)")
                // NOOP
                // Swallow the error as we have a seed file.
                // If this was necssary send a notification
                
                
            }
        }
    }
    
    
    /*  This  coombines the Alamofire/Moya/Sync frameworks to query
     *  the NYC Rest API. Thing to note is how  the mapping was done  to indicate  dbn  is the key.
     *  that is  done  it they coredata editor for the dbn property
     */
    
    
    class func postProcessImport(context:NSManagedObjectContext)  {
        print("Start School post process")
        context.perform {
            let fetchRequest:NSFetchRequest = School.fetchRequest()
            
            var schools:[School]! = nil
            // Edit the entity name as appropriate.
            let entity = NSEntityDescription.entity(forEntityName: "School", in: context)
            fetchRequest.entity = entity
            
            
            do {
                schools =  try context.fetch(fetchRequest)
                if schools.count > 0 {
                    
                    var counter1 = 0
                    var counter2 = 0
                    var counter3 = 0
                    var counter4 = 0

                    var tempNum = 0

                    var avg_num_of_sat_test_takers = 0
                    var avg_sat_critical_reading_avg_score =  0
                    var avg_sat_math_avg_score = 0
                    var avg_sat_writing_avg_score =  0
                    
                    for school in schools {
                        
                        let sc = School.schoolScoreByDbn(dbn: school.dbn! , context: context)
                        
                        sc?.school = school
                        school.score = sc
                        
                        
                        if (school.score != nil && (school.score?.num_of_sat_test_takers?.isInt)! ){
                            counter1 += 1

                            tempNum = Int(school.score!.num_of_sat_test_takers!)!
                            avg_num_of_sat_test_takers += tempNum
                            
                        }
                        
                        if (school.score != nil && (school.score?.sat_math_avg_score?.isInt)! ){
                            counter2 += 1

                            tempNum = Int(school.score!.sat_math_avg_score!)!
                            avg_sat_math_avg_score += tempNum
                        }
                        
                        if (school.score != nil && (school.score?.sat_writing_avg_score?.isInt)! ){
                            counter3 += 1

                            tempNum = Int(school.score!.sat_writing_avg_score!)!
                            avg_sat_writing_avg_score += tempNum
                            
                        }

                        if (school.score != nil && (school.score?.sat_critical_reading_avg_score?.isInt)! ){
                            counter4 += 1

                            tempNum = Int(school.score!.sat_critical_reading_avg_score!)!
                            avg_sat_critical_reading_avg_score += tempNum
                            
                        }
                    }
                    
                    avg_num_of_sat_test_takers /= counter1
                    avg_sat_critical_reading_avg_score /= counter2
                    avg_sat_math_avg_score /= counter3
                    avg_sat_writing_avg_score /= counter4
                    
                    if let city = cityByName(name: "New York", context: context) {
                        city.avg_sat_math_avg_score =  Int16(avg_sat_math_avg_score)
                        city.avg_sat_critical_reading_avg_score = Int16(avg_sat_critical_reading_avg_score)
                        city.avg_sat_writing_avg_score = Int16(avg_sat_writing_avg_score)
                        city.avg_num_of_sat_test_takers = Int16(avg_num_of_sat_test_takers)
                    }else{
                        let entity = NSEntityDescription.entity(forEntityName: "City", in: context)
                        let newCity = City.init(entity: entity!, insertInto: context)
                        newCity.name = "New York"
                        newCity.avg_sat_math_avg_score =  Int16(avg_sat_math_avg_score)
                        newCity.avg_sat_critical_reading_avg_score = Int16(avg_sat_critical_reading_avg_score)
                        newCity.avg_sat_writing_avg_score = Int16(avg_sat_writing_avg_score)
                        newCity.avg_num_of_sat_test_takers = Int16(avg_num_of_sat_test_takers)
                        
                    }
                    
                    
                    try context.save()

                   
                    
                    print("End Post Process")

                }
                
                
            } catch {
                print("Failed Post Process")
                print("Unresolved error \(error)")
            }
        }
    }
    
    class func schoolScoreByDbn(dbn:String, context:NSManagedObjectContext) -> SchoolScore? {
        
        let fetchRequest = NSFetchRequest<SchoolScore>()
        let entityDescription = NSEntityDescription.entity(forEntityName: "SchoolScore", in: context)
        
        fetchRequest.entity = entityDescription
        
        
        
        fetchRequest.predicate =  NSPredicate(format: "dbn = %@", dbn)
        let sortDescriptor = NSSortDescriptor(key: "dbn", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        // Edit the section name key path and cache name if appropriate.
        // nil for section name key path means "no sections".
        let aFetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: context, sectionNameKeyPath: nil, cacheName:nil)
        
        do {
            try aFetchedResultsController.performFetch()
            return aFetchedResultsController.fetchedObjects?.first
            
        } catch {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            //print("Unresolved error \(error), \(error.userInfo)")
            abort()
        }
    }
    
    class func cityByName(name:String, context:NSManagedObjectContext) -> City? {
        
        let fetchRequest = NSFetchRequest<City>()
        let entityDescription = NSEntityDescription.entity(forEntityName: "City", in: context)
        
        fetchRequest.entity = entityDescription
        
        
        
        fetchRequest.predicate =  NSPredicate(format: "name = %@", name)
        let sortDescriptor = NSSortDescriptor(key: "name", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        // Edit the section name key path and cache name if appropriate.
        // nil for section name key path means "no sections".
        let aFetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: context, sectionNameKeyPath: nil, cacheName:nil)
        
        do {
            try aFetchedResultsController.performFetch()
            return aFetchedResultsController.fetchedObjects?.first
            
        } catch {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            //print("Unresolved error \(error), \(error.userInfo)")
            abort()
        }
    }
    
    
}
