//
//  SchoolScore+CoreDataProperties.swift
//  
//
//  Created by James Cicenia on 10/20/18.
//
//

import Foundation
import CoreData


extension SchoolScore {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<SchoolScore> {
        return NSFetchRequest<SchoolScore>(entityName: "SchoolScore")
    }

    @NSManaged public var num_of_sat_test_takers: String?
    @NSManaged public var sat_critical_reading_avg_score: String?
    @NSManaged public var sat_math_avg_score: String?
    @NSManaged public var sat_writing_avg_score: String?
    @NSManaged public var dbn: String?
    @NSManaged public var school: School?

}
