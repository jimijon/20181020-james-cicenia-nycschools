//
//  Extensions.swift
//  NewYorkCitySchools
//
//  Created by James Cicenia on 10/21/18.
//  Copyright © 2018 jimijon.com. All rights reserved.
//

import Foundation
import UIKit

extension String {

    func stringByAppendingPathComponent(_ path: String) -> String {
        let nsSt = self as NSString
        return nsSt.appendingPathComponent(path)
    }
    
    var isInt: Bool {
        return Int(self) != nil
    }
    

}

extension AppDelegate {
    
    func copyBundledSQLiteDB(source:String, destination:String) {
        
        
        let sourcePath = Bundle.main.path(forResource:source, ofType:"sqlite")
        if(sourcePath == nil){
            print (source+" file doesn't exist")
            return
        }
        let documentDirectoryPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first
        
        let destinationPath = documentDirectoryPath?.stringByAppendingPathComponent(destination+".sqlite")
        
        if !FileManager().fileExists(atPath: destinationPath!) {
            do {
                try FileManager().copyItem(atPath: sourcePath!, toPath: destinationPath!)
                
            } catch _ {
                print ("You need to include the seed database")
                abort()
            }
        }
        
    }
}

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        tap.cancelsTouchesInView = false
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}

