//
//  SchoolDetailViewController.swift
//  NewYorkCitySchools
//
//  Created by James Cicenia on 10/20/18.
//  Copyright © 2018 jimijon.com. All rights reserved.
//

import UIKit

class SchoolDetailViewController: UIViewController {

    @IBOutlet weak var schoolNameLbl:UILabel!
    @IBOutlet weak var schoolBoroughLbl:UILabel!
    
    @IBOutlet weak var satCriticalReadingAvgScoreLbl:UILabel!
    @IBOutlet weak var satMathAvgScoreLbl:UILabel!
    @IBOutlet weak var satWritingAvgScoreLbl:UILabel!
    @IBOutlet weak var numOfTestTakersLbl:UILabel!

    @IBOutlet weak var avgNumOfTestTakersLbl:UILabel!
    @IBOutlet weak var avgSatCriticalReadingAvgScoreLbl:UILabel!
    @IBOutlet weak var avgSatMathAvgScoreLbl:UILabel!
    @IBOutlet weak var avgSatWrittingAvgScoreLbl: UILabel!
    
    var school:School!
    let appDelegate = UIApplication.shared.delegate as! AppDelegate

    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func  viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let badScore = "n/a"

        
        if  let city = School.cityByName(name: "New York", context: appDelegate.dataStack.mainContext){
            avgNumOfTestTakersLbl.text  =  "\(city.avg_num_of_sat_test_takers)"
            avgSatCriticalReadingAvgScoreLbl.text =  "\(city.avg_sat_critical_reading_avg_score)"
            avgSatMathAvgScoreLbl.text =  "\(city.avg_sat_math_avg_score)"
            avgSatWrittingAvgScoreLbl.text = "\(city.avg_sat_writing_avg_score)"
        }else{
            avgNumOfTestTakersLbl.text  =  badScore
            avgSatCriticalReadingAvgScoreLbl.text =  badScore
            avgSatMathAvgScoreLbl.text =  badScore
            avgSatWrittingAvgScoreLbl.text = badScore
        }
        
        schoolNameLbl.text = school.school_name
        schoolBoroughLbl.text = school.borough
        
        if (school.score != nil && (school.score?.num_of_sat_test_takers?.isInt)! ){
            numOfTestTakersLbl.text = school.score?.num_of_sat_test_takers
        }else{
            numOfTestTakersLbl.text = badScore
        }
        
        if (school.score != nil && (school.score?.sat_math_avg_score?.isInt)! ){
            satMathAvgScoreLbl.text = school.score?.sat_math_avg_score
        }else{
            satMathAvgScoreLbl.text = badScore
        }
        
        if (school.score != nil && (school.score?.sat_writing_avg_score?.isInt)! ){
            satWritingAvgScoreLbl.text = school.score?.sat_writing_avg_score
        }else{
            satWritingAvgScoreLbl.text = badScore
        }
        
        if (school.score != nil && (school.score?.sat_critical_reading_avg_score?.isInt)! ){
            satCriticalReadingAvgScoreLbl.text = school.score?.sat_critical_reading_avg_score
        }else{
            satCriticalReadingAvgScoreLbl.text = badScore
        }
        
    }
    
}
