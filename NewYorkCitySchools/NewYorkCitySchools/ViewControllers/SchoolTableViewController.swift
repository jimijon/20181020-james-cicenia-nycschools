//
//  SchoolTableViewController.swift
//  NewYorkCitySchools
//
//  Created by James Cicenia on 10/20/18.
//  Copyright © 2018 jimijon.com. All rights reserved.
//

import UIKit
import Foundation
import CoreData

class SchoolTableViewController: UIViewController,  UITableViewDelegate, UITableViewDataSource,  NSFetchedResultsControllerDelegate, UITextFieldDelegate {
    
    @IBOutlet weak var tableView:UITableView!
    @IBOutlet weak var searchTextField:UITextField!


    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var _searchFetchedResultsController: NSFetchedResultsController<School>? = nil

    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchTextField.delegate = self
        hideKeyboardWhenTappedAround()
        
        
        tableView.estimatedRowHeight = 44
        tableView.rowHeight = UITableView.automaticDimension

    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
    }
    
    
    //MARK:  -- TableView
    
    func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        return  searchFetchedResultsController.sectionIndexTitles;
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return searchFetchedResultsController.sections!.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return searchFetchedResultsController.sectionIndexTitles[section] as String
    }
    
    func tableView(_ tableView: UITableView, sectionForSectionIndexTitle title: String, at index: Int) -> Int {
        
        if(index > 0){
            return (searchFetchedResultsController.sectionIndexTitles as [String]).index(of: title)!
            
        }else{
            return NSNotFound
        }
    }
    
    // Create and Style Custom Section Header
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        let view = UIView()
//        view.backgroundColor = appDelegate.lightColor
//
//        let keyline = UIView()
//        keyline.frame = CGRect(x: 0, y: 42, width: Int(tableView.frame.width), height: 2)
//        keyline.backgroundColor = appDelegate.mediumColor
//        view.addSubview(keyline)
//
//        let label = UILabel()
//        label.text = self.tableView(self.tableView, titleForHeaderInSection: section)
//        label.frame = CGRect(x: 16, y: 0, width: Int(tableView.frame.width - 32), height: 44)
//        label.font = UIFont(name: "AvenirNextCondensed-DemiBold", size: 24)
//        label.textColor = appDelegate.mediumColor
//        view.addSubview(label)
//
//        return view
//    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44
    }

    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        let vc = storyboard!.instantiateViewController(withIdentifier: "SchoolDetailViewController") as! SchoolDetailViewController

        vc.school  = _searchFetchedResultsController?.object(at: indexPath)
        let backButtonText = ""
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:backButtonText, style: UIBarButtonItem.Style.plain, target: nil, action: nil)

//        clearSearch = false
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var numberOfRows = 0;
        let fetchController = searchFetchedResultsController;
        let sections = fetchController.sections;
        
        if(sections!.count > 0)
        {
            let sectionInfo = sections![section] as NSFetchedResultsSectionInfo
            numberOfRows = sectionInfo.numberOfObjects;
        }
        return numberOfRows;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "SchoolTableViewCell", for: indexPath) as! SchoolTableViewCell
        
        let school = searchFetchedResultsController.object(at: indexPath)
        
        cell.schoolNameLbl.text = school.school_name
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    
    //MARK: -- Search Handling
    //Search Field Handling
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        _searchFetchedResultsController = nil
        tableView.reloadData()
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        searchTextField.text = ""
        _searchFetchedResultsController = nil
        tableView.reloadData()
        return true
    }
    
    var searchFetchedResultsController: NSFetchedResultsController <School>{
        if _searchFetchedResultsController != nil {
            return _searchFetchedResultsController!
        }
        
        let fetchRequest: NSFetchRequest<School> = School.fetchRequest()
        
        // Edit the entity name as appropriate.
        let entity = NSEntityDescription.entity(forEntityName: "School", in: appDelegate.dataStack.mainContext)
        fetchRequest.entity = entity
        
        // Set the batch size to a suitable number.
        fetchRequest.fetchBatchSize = 20
        
        var predicate = NSPredicate()

        
        var fetchPredicates = [NSPredicate]()
        

        
        if((searchTextField.text ?? "").count > 0){
            var queryString =  ""
            queryString = searchTextField.text!
            predicate = NSPredicate(format:"school_name contains[cd] %@", queryString)
       
            fetchPredicates.append(predicate)
        }
        var sortDescriptors = [NSSortDescriptor]()

        
        fetchRequest.predicate =  NSCompoundPredicate(type: .and, subpredicates: fetchPredicates)
        let sortDescriptor1 = NSSortDescriptor(key: "sectionKey", ascending: true, selector:#selector(NSString.caseInsensitiveCompare(_:)))
        let sortDescriptor2 = NSSortDescriptor(key: "school_name", ascending: true, selector:#selector(NSString.caseInsensitiveCompare(_:)))
        sortDescriptors.append(sortDescriptor1)
        sortDescriptors.append(sortDescriptor2)

        fetchRequest.sortDescriptors = sortDescriptors
        
        let aFetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: appDelegate.dataStack.mainContext, sectionNameKeyPath: "sectionKey", cacheName:nil)
        
        aFetchedResultsController.delegate = self
        _searchFetchedResultsController = aFetchedResultsController
        
        do {
            try _searchFetchedResultsController!.performFetch()
        } catch {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            //print("Unresolved error \(error), \(error.userInfo)")
            abort()
        }
        
        return _searchFetchedResultsController!
    }



}

